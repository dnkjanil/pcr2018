from django import forms


class StaticPCRProcessForm(forms.Form):
    cycle_count = forms.IntegerField(required=True, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Jumlah Siklus'
        }
    ))

class DynamicPCRProcessForm(forms.Form):
    step = forms.IntegerField(required=False, widget=forms.NumberInput(
        attrs={
            'readonly': '',
            'class': 'form-control',
            'placeholder': 'Step',
            'required': ''
        }
    ))
    temperature = forms.FloatField(required=False, widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Temperature',
            'required': '',
            'min': 0
        }
    ))
    process_time = forms.IntegerField(required=False, widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Process Time (Second)',
            'required': '',
            'min': 1
        }
    ))
    cycle_count = forms.IntegerField(required=False, widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Cycle Count',
            'required': '',
            'min': 1
        }
    ))
    go_to_step = forms.IntegerField(required=False, widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Go To Step',
            'required': '',
            'min': 1
        }
    ))
    go_to_count = forms.IntegerField(required=False, widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Go To Count',
            'required': '',
            'min': 1
        }
    ))


# Formset for dynamic form
DynamicPCRProcessFormSet = forms.formset_factory(DynamicPCRProcessForm, extra=0)
