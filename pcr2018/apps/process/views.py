from django.shortcuts import render, reverse
from pcr2018.apps.info.utils import ResponseGenerator
from django.shortcuts import HttpResponse, HttpResponseRedirect
from pcr2018.apps.process.forms import StaticPCRProcessForm
from pcr2018.apps.process.models import ProcessStatusStatic, StepMapper, NormalStep, GoToStep, ParsedStep
import sweetify
import multiprocessing
from pcr2018.apps.driver.relay import PCRBridge
import time
from pcr2018.apps.process.forms import DynamicPCRProcessFormSet
import json

# Recursive parser
def recursive_parser(from_step, to_step):
    # Iterate through the steps
    for i in range(from_step, to_step + 1):
        print(i)
        step = StepMapper.objects.get(step=str(i))
        # If it is normal step
        if step.step_type == '1':
            step_detail = NormalStep.objects.get(step=step)
            text = 'Hold on {}c for {} second'.format(step_detail.temperature, step_detail.process_time)
            parsed_step = ParsedStep(temperature=step_detail.temperature, cycle_count=step_detail.cycle_count, process_time=step_detail.process_time, process_text=text)
            parsed_step.save()
        # If it is go to step
        else:
            step_detail = GoToStep.objects.get(step=step)
            # Iterates through the go_to count
            for j in range(1, step_detail.go_to_count+1):
                recursive_parser(int(step_detail.go_to_step), i-1)



# Process for mantaining cover heater
def cover_heater_worker():
    # Keep penutup heater at 95 degree
    status = ProcessStatusStatic.objects.latest('id')
    status.running = True
    status.current_process = 'Cover heater starting'
    status.save()
    bridge = PCRBridge()
    bridge.hold_penutup_temp(105)


# Process for running the cycle
def cycle_worker():
    # TODO : Implement go to step
    # Set the status
    status = ProcessStatusStatic.objects.latest('id')
    status.running = True
    status.current_process = 'Starting'
    status.save()

    bridge = PCRBridge()
    # Get the steps
    parsed_steps = ParsedStep.objects.all()
    for step in parsed_steps:
        for cycle in range(0, int(step.cycle_count)):
            try:
                status = ProcessStatusStatic.objects.latest('id')
                status.current_process = step.process_text + ', Cycle {}'.format(cycle + 1)
                status.save()
                bridge.hold_temp(step.temperature, step.process_time)
            except:
                pass

    # If it is stopped
    # Retake the status
    status = ProcessStatusStatic.objects.latest('id')
    if status.current_process == 'Stopped':
        pass
    else:
        status.current_process = 'Finished'
        status.running = False
        status.save()


# Process for running the cycle
def static_cycle_worker(cycle_count):
    # The amplification protocol consisted of 10 min at 95uC,
    # followed by 40 cycles of amplification (95uC for 5 s, 54uC for 5 s,
    # 72uC for 15 s). Subsequently, the reaction was stopped at 95uC for
    # 2 minutes, cooled (20uC for 1 min) and melted (70–94uC with
    # plate readings set at 0.5uC).

    bridge = PCRBridge()
    status = ProcessStatusStatic.objects.latest('id')
    status.running = True
    status.current_process = 'Starting'
    status.save()


    try:
        status = ProcessStatusStatic.objects.latest('id')
        status.current_process = '95c for 120 second'
        status.save()
        bridge.hold_temp(95, 120)
    except:
        pass

    for i in range(1,31):
        try:
            status = ProcessStatusStatic.objects.latest('id')
            status.current_process = '95c for 30 second, Cycle: {}'.format(i)
            status.save()
            bridge.hold_temp(95, 30)
        except:
            pass

        try:
            status = ProcessStatusStatic.objects.latest('id')
            status.current_process = '55c for 30 second, Cycle: {}'.format(i)
            status.save()
            bridge.hold_temp(55, 30)
        except:
            pass

        try:
            status = ProcessStatusStatic.objects.latest('id')
            status.current_process = '72c for 60 second, Cycle: {}'.format(i)
            status.save()
            bridge.hold_temp(72, 60)
        except:
            pass

    try:
        status = ProcessStatusStatic.objects.latest('id')
        status.current_process = '72c for 300 second, Cycle: {}'.format(i)
        status.save()
        bridge.hold_temp(72, 300)
    except:
        pass

    # If it is stopped
    # Retake the status
    status = ProcessStatusStatic.objects.latest('id')
    if status.current_process == 'Stopped':
        pass
    else:
        status.current_process = 'Finished'
        status.running = False
        status.save()

    # Todo : What to do in the last step ?


# Create your views here.
def static_pcr_process(request):
    if request.method == "GET":
        form = StaticPCRProcessForm()
        context = {
            'form': form
        }
        return render(request, 'static_process.html', context)
    else:
        form = StaticPCRProcessForm(request.POST)
        if form.is_valid():
            m = multiprocessing.Process(target=static_cycle_worker, args=(form.cleaned_data['cycle_count'],))
            p = multiprocessing.Process(target=cover_heater_worker, args=())
            m.start()
            p.start()
            sweetify.info(request, 'Proses berhasil dimulai')
            return HttpResponseRedirect(reverse('static_pcr_process'))

        else:
            sweetify.error(request, 'Jumlah siklus tidak valid')
            return HttpResponseRedirect(reverse('static_pcr_process'))


def pcr_process(request):
    if request.method == "GET":
        # Get all steps
        steps = StepMapper.objects.all()
        # Formset initial value
        initial_value = []
        for step in steps:
            # Normal step
            if step.step_type == "1":
                step_detail = NormalStep.objects.get(step=step)
                value = {
                    'step': step.step,
                    'temperature': step_detail.temperature,
                    'process_time': step_detail.process_time,
                    'cycle_count': step_detail.cycle_count
                }
                initial_value.append(value)
            # Goto step
            elif step.step_type == "2":
                step_detail = GoToStep.objects.get(step=step)
                value = {
                    'step': step.step,
                    'go_to_step': step_detail.go_to_step,
                    'go_to_count': step_detail.go_to_count
                }
                initial_value.append(value)

        forms = DynamicPCRProcessFormSet(initial=initial_value)
        context = {
            'forms': forms
        }
        return render(request, 'dynamic_process.html', context)

    elif request.method == 'POST':
        # Check for current process
        if not ProcessStatusStatic.objects.latest('id').running:
            forms = DynamicPCRProcessFormSet(request.POST)
            # Iterate through the forms
            if forms.is_valid():
                if forms.cleaned_data:
                    # Clear the current steps
                    steps = StepMapper.objects.all()
                    for step in steps:
                        step.delete()
                    # Remove parsed steps
                    parsed_steps = ParsedStep.objects.all()
                    for parsed_step in parsed_steps:
                        parsed_step.delete()

                    for form in forms:
                        # Normal step
                        if (form.cleaned_data['temperature']):
                            step = StepMapper(step=str(form.cleaned_data['step']), step_type='1')
                            step.save()
                            normal_step = NormalStep(step=step, temperature=float(form.cleaned_data['temperature']),
                                                     cycle_count=int(form.cleaned_data['cycle_count']),
                                                     process_time=float(form.cleaned_data['process_time']))
                            normal_step.save()
                        else:
                            # Check if the step is correct
                            if not(form.cleaned_data['go_to_step'] > len(forms.cleaned_data)):
                                step = StepMapper(step=str(form.cleaned_data['step']), step_type='2')
                                step.save()
                                go_to_step = GoToStep(step=step, go_to_step=str(form.cleaned_data['go_to_step']),
                                                      go_to_count=int(form.cleaned_data['go_to_count']))
                                go_to_step.save()
                            else:
                                sweetify.error(request, 'Invalid step submitted')
                                return HttpResponseRedirect(reverse('pcr_process'))

                    # Prepare recursive parser
                    step_count = StepMapper.objects.all().count()
                    recursive_parser(1, step_count)
                    # Start Main process
                    m = multiprocessing.Process(target=cycle_worker, args=())
                    p = multiprocessing.Process(target=cover_heater_worker, args=())
                    m.start()
                    p.start()
                    sweetify.success(request, 'Process successfully executed')
                    return HttpResponseRedirect(reverse('pcr_process'))
                else:
                    sweetify.error(request, 'Please add at least one step')
                    return HttpResponseRedirect(reverse('pcr_process'))
        else:
            sweetify.error(request, 'Please stop ongoing process first')
            return HttpResponseRedirect(reverse('pcr_process'))


def stop_process(request):
    if request.method == "GET":
        # Retrieve the status
        status = ProcessStatusStatic.objects.latest('id')
        status.current_process = 'Stopped'
        status.running = False
        status.save()
        sweetify.success(request, 'Process successfully stopped')
        return HttpResponseRedirect(reverse('pcr_info'))
    else:
        sweetify.error(request, 'Invalid Request')
        return HttpResponseRedirect(reverse('pcr_info'))


def clear_process(request):
    if request.method == "GET":
        # Retrieve the status
        status = ProcessStatusStatic.objects.latest('id')
        status.current_process = 'Stopped'
        status.running = False
        status.save()
        # Remove all step
        steps = StepMapper.objects.all()
        for step in steps:
            step.delete()
        # Remove parsed steps
        parsed_steps = ParsedStep.objects.all()
        for parsed_step in parsed_steps:
            parsed_step.delete()
        sweetify.success(request, 'Process successfully cleared')
        return HttpResponseRedirect(reverse('pcr_process'))
    else:
        sweetify.error(request, 'Invalid Request')
        return HttpResponseRedirect(reverse('pcr_process'))