from django.contrib import admin
from pcr2018.apps.process.models import *

# Register your models here.
admin.site.register(ProcessStatusStatic)
admin.site.register(StepMapper)
admin.site.register(NormalStep)
admin.site.register(GoToStep)
admin.site.register(ParsedStep)

