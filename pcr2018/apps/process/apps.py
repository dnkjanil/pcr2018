from django.apps import AppConfig


class ProcessConfig(AppConfig):
    name = 'pcr2018.apps.process'
