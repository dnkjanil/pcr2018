from django.db import models

# Create your models here.
class ProcessStatusStatic(models.Model):
    current_process = models.CharField(max_length=250)
    # Process status
    running = models.BooleanField()
    time_updated = models.DateTimeField(auto_now=True)


class StepMapper(models.Model):
    step = models.CharField(max_length=20)
    # 1 For normal
    # 2 For goto
    step_type = models.CharField(max_length=1)

    def __str__(self):
        return self.step

class NormalStep(models.Model):
    step = models.ForeignKey(StepMapper, on_delete=models.CASCADE)
    temperature = models.FloatField()
    # Process time in second
    process_time = models.FloatField()
    # Cycle count
    cycle_count = models.IntegerField()

    def __str__(self):
        return str(self.temperature)

class GoToStep(models.Model):
    step = models.ForeignKey(StepMapper, on_delete=models.CASCADE)
    # go to step
    go_to_step = models.CharField(max_length=20)
    # go to count
    go_to_count = models.IntegerField()

    def __str__(self):
        return self.go_to_step

# Parsed step for easier linier execution
class ParsedStep(models.Model):
    temperature = models.FloatField()
    # Process time in second
    process_time = models.FloatField()
    # Cycle count
    cycle_count = models.IntegerField()
    process_text = models.CharField(max_length=100)

    def __str__(self):
        return str(self.temperature)







