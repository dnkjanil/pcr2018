from django.urls import path
from pcr2018.apps.process.views import static_pcr_process, stop_process, pcr_process, clear_process

urlpatterns = [
    path('static', static_pcr_process, name='static_pcr_process'),
    path('', pcr_process, name='pcr_process'),
    path('stop', stop_process, name='stop_process'),
    path('clear', clear_process, name='clear_process')
]
