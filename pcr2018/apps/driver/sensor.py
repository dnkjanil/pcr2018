import glob
import time

class PCRSensor:
    def __init__(self):
        # Every ds18b20 sensor had their own id
        self.main_sensor_id = '28-021565386aff'
        self.penutup_sensor_id = '28-0018984304d0'
        self.onewire_base_dir = '/sys/bus/w1/devices/'
        # File for each sensor
        self.main_sensor_file = glob.glob(self.onewire_base_dir + self.main_sensor_id)[0] + '/w1_slave'
        self.penutup_sensor_file = glob.glob(self.onewire_base_dir + self.penutup_sensor_id)[0] + '/w1_slave'

    # Read raw penutup temp
    def read_main_temp_raw(self):
        f = open(self.main_sensor_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    # Read raw cover temp
    def read_cover_temp_raw(self):
        f = open(self.penutup_sensor_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    # read temp
    # taken and modified from http://www.circuitbasics.com/raspberry-pi-ds18b20-temperature-sensor-tutorial/

    # read cover temp
    def read_cover_temp(self):
        lines = self.read_cover_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_cover_temp_raw()
        equals_pos = lines[1].find('t=')

        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            temp_c = float(temp_string) / 1000.0
            return temp_c

    # read main temp
    def read_main_temp(self):
        lines = self.read_main_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_main_temp_raw()
        equals_pos = lines[1].find('t=')

        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            temp_c = float(temp_string) / 1000.0
            return temp_c
