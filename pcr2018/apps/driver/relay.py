from pcr2018.apps.driver.sensor import PCRSensor
import gpiozero
import time
from pcr2018.apps.process.models import ProcessStatusStatic

class PCRBridge():
    def __init__(self):

        # GPIO 17 -> Relay Pin (Main peltier + fan)
        # GPIO 10 -> Relay Pin (Cover Peltier)
        # 3.3v always on -> pwm h-bridge
        # GPIO 27 -> IN B H-Bridge (Cold)
        # GPIO 22 -> IN A H-Bridge (Heat)

        # Create new process object
        self.process_status = ProcessStatusStatic

        self.RELAY_PIN = 17
        self.H_BRIDGE_COLD_PIN = 27
        self.H_BRIDGE_HEAT_PIN = 22
        self.COVER_RELAY_PIN = 10

        # Temp
        self.sensor = PCRSensor()

        # Relay
        self.relay = gpiozero.OutputDevice(self.RELAY_PIN, active_high=False, initial_value=False)
        self.penutup_relay = gpiozero.OutputDevice(self.COVER_RELAY_PIN, active_high=False, initial_value=False)
        # Peltiers
        self.peltier_cold = gpiozero.OutputDevice(self.H_BRIDGE_COLD_PIN, initial_value=False)
        self.peltier_heat = gpiozero.OutputDevice(self.H_BRIDGE_HEAT_PIN, initial_value=False)

    def cover_go_to_temp(self, temp):
        # Turn relay on
        current_temp = self.sensor.read_cover_temp()
        while (current_temp < float(temp)) and self.process_status.objects.latest('id').running:
            self.penutup_relay.on()
            current_temp = self.sensor.read_cover_temp()
        self.penutup_relay.off()

    def go_to_temp_grad(self, temp):
        # Turn relay on
        self.relay.on()
        current_temp = self.sensor.read_main_temp()
        if current_temp < temp:
            while (current_temp < float(temp-5)) and self.process_status.objects.latest('id').running:
                self.peltier_heat.on()
                time.sleep(1)
                current_temp = self.sensor.read_main_temp()
                self.peltier_heat.off()
                time.sleep(5)
            self.clean_up()
        else:
            while (current_temp > float(temp)) and self.process_status.objects.latest('id').running:
                self.peltier_cold.on()
                current_temp = self.sensor.read_main_temp()
            self.clean_up()

    def clean_up(self):
        # Safely turn everything off
        self.peltier_cold.off()
        self.peltier_heat.off()
        self.relay.off()

    def go_to_temp(self, temp):
        # Turn relay on
        self.relay.on()
        current_temp = self.sensor.read_main_temp()
        if current_temp < temp:
            while (current_temp < float(temp)) and self.process_status.objects.latest('id').running:
                self.peltier_heat.on()
                current_temp = self.sensor.read_main_temp()
            self.peltier_heat.off()
        else:
            while (current_temp > float(temp)) and self.process_status.objects.latest('id').running:
                self.peltier_cold.on()
                current_temp = self.sensor.read_main_temp()
            self.peltier_cold.off()

    # Todo: fix the redudancy
    def go_to_temp_timed(self, temp, timeout):
        max_time = time.time() + timeout
        current_time = time.time()
        # Turn relay on
        self.relay.on()
        current_temp = self.sensor.read_main_temp()
        if current_temp < temp:
            while (current_temp < float(temp)) and self.process_status.objects.latest('id').running and (max_time - current_time) >= 0:
                self.peltier_heat.on()
                current_time = time.time()
                current_temp = self.sensor.read_main_temp()
            self.peltier_heat.off()
        else:
            while (current_temp > float(temp)) and self.process_status.objects.latest('id').running and (max_time - current_time) >= 0:
                self.peltier_cold.on()
                current_time = time.time()
                current_temp = self.sensor.read_main_temp()
            self.peltier_cold.off()

    def hold_temp(self, temp, time_to_hold):
        # Make the temp go near the temperature to hold
        self.go_to_temp(temp)
        target_time = time.time() + time_to_hold
        current_time = time.time()

        while ((target_time - current_time) >= 0) and self.process_status.objects.latest('id').running:
            self.go_to_temp_timed(temp, target_time - current_time)
            current_time = time.time()
        self.clean_up()

    def hold_penutup_temp(self, temp):
        while  self.process_status.objects.latest('id').running:
            self.cover_go_to_temp(temp)
        self.penutup_relay.off()
