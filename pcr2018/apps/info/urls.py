from django.urls import path
from pcr2018.apps.info.views import get_temp_info

urlpatterns = [
    path('temp', get_temp_info, name='get_temp_info'),
]