from pcr2018.apps.info.utils import ResponseGenerator
from django.shortcuts import HttpResponse, HttpResponseRedirect, reverse, render
import json
from pcr2018.apps.driver.sensor import PCRSensor
import sweetify
from pcr2018.apps.process.models import ProcessStatusStatic

def get_temp_info(request):
    # Classes
    rg = ResponseGenerator()
    sensor = PCRSensor()
    status = ProcessStatusStatic.objects.latest('id')
    if request.method == "GET":
        data = {
            'main_current_temp': sensor.read_main_temp(),
            'cover_current_temp': sensor.read_cover_temp(),
            'current_process': status.current_process,
            'running': status.running
        }
        response_data = rg.data_response_generator(data=data)
        return HttpResponse(json.dumps(response_data), content_type='application/json', status=200)
    else:
        response_data = rg.error_response_generator(400, 'Bad request method')
        return HttpResponse(json.dumps(response_data), content_type='application/json', status=400)


#@def static_cycle_page

def pcr_info(request):
    # Classes
    rg = ResponseGenerator()
    sensor = PCRSensor()
    status = ProcessStatusStatic.objects.latest('id')
    if request.method == "GET":
        return render(request, 'pcr_info.html')
    else:
        sweetify.error(request, 'Request method is not valid')
        return HttpResponseRedirect(reverse('pcr_info'))