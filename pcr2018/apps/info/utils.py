from django.shortcuts import HttpResponse

class ResponseGenerator:
    # generate error response in dict format
    def error_response_generator(self, code, message):
        response = {
            "error": {
                "code": code,
                "message": message
            }
        }
        return response

    def file_response_generator(self, filename, content_type, content):
        response = HttpResponse(content, content_type=content_type)
        response['Content-Disposition'] = 'attachment; filename={}'.format(filename)
        return response

    # generate data response in dict format
    # input parameter data(dict)
    def data_response_generator(self, data):
        response = {
            "data": data
        }
        return response

    # generate error response in dict format
    def success_response_generator(self, code, message):
        response = {
            "success": {
                "code": code,
                "message": message
            }
        }
        return response
