from django.apps import AppConfig


class InfoConfig(AppConfig):
    name = 'pcr2018.apps.info'
